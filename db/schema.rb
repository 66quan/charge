# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180108154132) do

  create_table "Transactions", primary_key: "ID", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "AccountId"
    t.string "Address"
    t.integer "Token"
    t.integer "TokenAmount"
    t.decimal "Btc", precision: 28, scale: 10
    t.decimal "payAmount", precision: 28, scale: 10
    t.integer "status", default: 0
    t.integer "Type"
    t.integer "confirmations", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "last_txid"
    t.integer "unconfirmToken", default: 0
  end

  create_table "accounts", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.integer "failed_attempts", default: 0, null: false
    t.string "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "lock_btc"
    t.index ["confirmation_token"], name: "index_accounts_on_confirmation_token", unique: true
    t.index ["email"], name: "index_accounts_on_email", unique: true
    t.index ["reset_password_token"], name: "index_accounts_on_reset_password_token", unique: true
    t.index ["unlock_token"], name: "index_accounts_on_unlock_token", unique: true
  end

  create_table "btcAddress", id: false, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "Address", null: false
    t.integer "assign", limit: 1, default: 0
  end

  create_table "charges", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.bigint "account_id"
    t.string "wallet_type"
    t.string "wallet_id"
    t.decimal "amount", precision: 28, scale: 10
    t.integer "status", limit: 1, default: 1
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["account_id"], name: "index_charges_on_account_id"
    t.index ["wallet_type", "wallet_id"], name: "index_charges_on_wallet_type_and_wallet_id"
  end

  create_table "txes", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "address"
    t.decimal "amount", precision: 28, scale: 10
    t.integer "confirmations"
    t.integer "height"
    t.integer "satoshis"
    t.string "scriptPubKey"
    t.string "txid"
    t.integer "vout"
    t.integer "status", default: 0
    t.integer "paidToken", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "ts"
    t.index ["address", "txid"], name: "index_txes_on_address_and_txid"
  end

  create_table "wallet_btcs", id: :string, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "status", limit: 1, default: 1
    t.bigint "account_id"
    t.decimal "amount", precision: 28, scale: 10
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "PubKey"
    t.index ["account_id"], name: "index_wallet_btcs_on_account_id"
  end

  create_table "wallet_eths", id: :string, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "status", limit: 1, default: 1
    t.bigint "account_id"
    t.decimal "amount", precision: 28, scale: 10
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["account_id"], name: "index_wallet_eths_on_account_id"
  end

  create_table "wallet_ltcs", id: :string, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "status", limit: 1, default: 1
    t.bigint "account_id"
    t.decimal "amount", precision: 28, scale: 10
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["account_id"], name: "index_wallet_ltcs_on_account_id"
  end

  add_foreign_key "charges", "accounts"
  add_foreign_key "wallet_btcs", "accounts"
  add_foreign_key "wallet_eths", "accounts"
  add_foreign_key "wallet_ltcs", "accounts"
end
