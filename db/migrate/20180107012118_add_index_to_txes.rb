class AddIndexToTxes < ActiveRecord::Migration[5.1]
  def change
    add_index :txes, [:address, :txid]
  end
end
