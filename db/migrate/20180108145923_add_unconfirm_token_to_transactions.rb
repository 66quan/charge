class AddUnconfirmTokenToTransactions < ActiveRecord::Migration[5.1]
  def change
    add_column :Transactions, :unconfirmToken, :integer, default: 0
  end
end
