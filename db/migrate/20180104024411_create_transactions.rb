class CreateTransactions < ActiveRecord::Migration[5.1]
  def change
    create_table :Transactions, primary_key: :ID do |t|
      t.integer :AccountId
      t.string :Address
      t.integer :Token
      t.integer :TokenAmount
      t.decimal :Btc, precision: 28, scale: 10
      t.decimal :payAmount, precision: 28, scale: 10
      t.integer :status, default: 0
      t.integer :Type
      t.integer :confirmations, default: 0
      t.timestamps
    end
  end
end
