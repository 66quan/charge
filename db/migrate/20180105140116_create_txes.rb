class CreateTxes < ActiveRecord::Migration[5.1]
  def change
    create_table :txes do |t|
      t.string :address
      t.decimal :amount, precision: 28, scale: 10
      t.integer :confirmations
      t.integer :height
      t.integer :satoshis
      t.string :scriptPubKey
      t.string :txid
      t.integer :vout
      t.integer :status, default: 0
      t.integer :paidToken, default: 0

      t.timestamps
    end
  end
end
