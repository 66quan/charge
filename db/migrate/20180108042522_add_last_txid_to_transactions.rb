class AddLastTxidToTransactions < ActiveRecord::Migration[5.1]
  def change
    add_column :Transactions, :last_txid, :string
  end
end
