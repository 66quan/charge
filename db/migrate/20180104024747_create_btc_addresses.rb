class CreateBtcAddresses < ActiveRecord::Migration[5.1]
  def change
    create_table :btcAddress, id: false do |t|
      t.string :Address, null: false, primary: true
      t.integer :assign, default: 0, limit: 1

      # t.timestamps
    end

    # execute "ALTER TABLE btcAddress add PRIMARY KEY (Address);"
  end
end
