class Blockexplorer
  def self.url(code)
    "#{SETTINGS['API_HOST']}/api/addr/#{code}/utxo?noCache=1"
  end

  def self.get_trade(code)
    JSON.parse RestClient.get url(code)
  rescue => err
    Rails.logger.info err
    nil
  end
end
