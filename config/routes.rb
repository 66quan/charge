Rails.application.routes.draw do

  match 'import/btc', via: [:get, :post]

  root to: 'home#index'

  get 'wallet' => 'home#wallet', as: :wallet
  put 'wallet/status' => 'home#status', as: :wallet_status
  get 'wallet/history' => 'home#history', as: :wallet_history
  get 'wallet/:address/txes' => 'home#txes', as: :wallet_txes
  match 'purchase' => 'home#purchase', as: :purchase, via: [:get, :put, :post]

  get 'api/btc/confirm'
  post 'api/btc/fresh'

  resources :charges

  devise_for :accounts, controllers: {
    registrations: 'account/registrations'
  }
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
