class Transaction < ApplicationRecord

  enum status: {no_receive: 0, unconfirm: 1, confirm: 2, finish: 3, fails: 9}

  self.table_name = 'Transactions'
  self.primary_key = 'ID'

  belongs_to :account, foreign_key: 'AccountId'

  before_save :set_status

  def set_status
    return if self.changed.include?('status')

    if self.confirmations == 0 && self.payAmount == 0 && self.unconfirmToken == 0
      self.status = :no_receive
    elsif self.confirmations < 6 && (self.payAmount > 0 || self.unconfirmToken > 0)
      self.status = :unconfirm
    else
      if self.payAmount == self.Btc
        self.status = :finish
      elsif self.payAmount > 0
        self.status = :confirm
      else
        self.status = :fails
      end
    end
  end

  def fresh
    trades = Blockexplorer.get_trade(self.Address)
    has_token = true
    if trades
      last_txid = nil
      trades.reverse.each do |trade|
        last_txid = trade['txid'] if trade['confirmations'] > 6

        tx = txes.where(txid: trade['txid']).first_or_initialize
        tx.attributes = trade.slice *tx.attributes.keys

        if tx.finish?
          next
        end

        self.confirmations = tx.confirmations if tx.confirmations > self.confirmations

        if tx.confirmations > 6
          if has_token && (token = Transaction.enable_token) > 0
            Transaction.transaction do
              self.payAmount += trade['amount'] if tx.pending?

              paid_token = trade['amount'] *  BtcAddress::TOKEN_UNIT - tx.paidToken
              if paid_token > token
                tx.paidToken += token
                self.TokenAmount += token
                tx.unfinish!
              else
                tx.paidToken += paid_token
                self.TokenAmount += paid_token
                tx.finish!
              end
              self.save
            end
          else
            has_token = false
            tx.unfinish!
            self.save
          end
        end

        tx.save
      end

      self.unconfirmToken = trades.sum{|s|
        if s['confirmations'] > 6
          0
          # s['amount'] * BtcAddress::TOKEN_UNIT
        else
          s['amount'] * BtcAddress::TOKEN_UNIT
        end
      }

      self.last_txid = last_txid
      self.save
    end
  end

  def delay_fresh
    TransactionJob.perform_later self.ID
  end

  def self.fresh_unconfirm
    Rails.logger.info "begin unconfirm transaction fresh"
    unconfirm.each &:fresh
    Rails.logger.info "end unconfirm transaction fresh"
  end

  def self.enable_token
    token = SETTINGS['TOKEN_FOR_PURCHASE'] - Transaction.sum(:TokenAmount)
  end

  def txes
    @txes ||= Tx.where(address: self.Address)
  end

  def confirm_txes_count
    txes.finish.size
  end

  def txes_count
    txes.size
  end
end
