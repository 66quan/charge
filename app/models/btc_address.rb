class BtcAddress < ApplicationRecord
  enum assign: {enabled: 0, used: 1, lock_used: 2}

  alias_attribute :status, :assign

  self.table_name = 'btcAddress'
  self.primary_key = 'Address'

  TOKEN_UNIT = 10000000

  def self.get_enable_btc account
    while btc = enabled.first do
      if btc.btc_valid?
        account.lock_btc = btc.Address
        btc.lock_used!
        account.save!
        return btc.Address
      else
        btc.used!
      end
    end
  end

  def btc_valid?
    trades = Blockexplorer.get_trade self.Address
    trades.blank?
  end
end
