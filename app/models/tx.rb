class Tx < ApplicationRecord
  # pending 未处理
  # finish 处理完成，全部购买
  # 部分购买
  enum status: {pending: 0, finish: 1, unfinish: 2}

end
