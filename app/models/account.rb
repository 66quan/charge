class Account < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :transactions, foreign_key: :accountId

  def btcs
    transactions
  end

  def total_btc_token
    btcs.sum(:TokenAmount)
  end

  def unconfirmation_btc_token
    btcs.unconfirm.sum(:unconfirmToken)
  end

  def purchase_btc_wallet_code
    if self.lock_btc
      self.lock_btc
    else
      BtcAddress.transaction do
        wallet = BtcAddress.enabled.first
        self.lock_btc = wallet.Address
        wallet.lock_used!
        self.save!
        wallet.Address
      end
    end
  end

  def purchase_btc(code, amount)
    wallet = BtcAddress.find self.lock_btc

    if !wallet.used? && wallet.btc_valid?
      Transaction.transaction do
        tr = Transaction.create!({
          account: self,
          Address: self.lock_btc,
          Token: (amount.to_d * BtcAddress::TOKEN_UNIT).to_i,
          TokenAmount: 0,
          payAmount: 0,
          Btc: amount
        })
        self.update! lock_btc: nil
        wallet.used!

        # tr.fresh

        return {
          status: 'OK'
        }
      end
    else
      self.update! lock_btc: nil
      wallet.used!
      {
        status: 'ERROR',
        msg: 'Wallet address invalid, please try again after refresh'
      }
    end
  end


end
