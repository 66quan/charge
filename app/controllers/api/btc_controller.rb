class Api::BtcController < ActionController::Base

  def fresh
    transaction = Transaction.find_by Address: params[:address]
    #if transaction.last_txid != params[:txid]
    #  transaction.update_columns last_txid:  params[:txid]
    transaction.fresh
    #end

    render json: {status: :OK }
  end

  def confirm
    params[:status] ||= 'success'
    if params[:code]
      charge = Charge.btcs.where(wallet_id: params[:code]).first
      if charge
        if charge.pending?
          if params[:status] = 'success'
            charge.success!
          else
            charge.failure!
          end
          render json: {status: :OK, msg: 'success'}
        else
          render json: {status: :ERROR, msg: 'status invalid'}
        end
      else
        render json: {status: :ERROR, msg: 'params invalid'}
      end
    else
      render json: {status: :ERROR, msg: 'params invalid'}
    end
  end
end
