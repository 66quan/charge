class ChargesController < ApplicationController
  before_action :set_charge, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @charges = current_account.charges.page(params[:page])
    if params[:wallet_type].present?
      @charges = @charges.where wallet_type: params[:wallet_type]
    end
    if params[:status].present?
      @charges = @charges.where status: params[:status]
    end
    respond_with(@charges)
  end

  def show
    # redirect_to charges_path
    respond_with(@charge)
  end

  def new
    @charge = Charge.new
    respond_with(@charge)
  end

  def edit
  end

  def create
    @charge = current_account.charges.new(charge_params)
    @charge.save
    respond_with(@charge, notice: '购买申请提交成功')
  end

  def update
    @charge.update(charge_params)
    respond_with(@charge)
  end

  def destroy
    @charge.destroy
    respond_with(@charge)
  end

  private
    def set_charge
      @charge = Charge.find(params[:id])
    end

    def charge_params
      params.require(:charge).permit(:account_id, :amount, :wallet_type)
    end
end
