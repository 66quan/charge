class HomeController < ApplicationController
  skip_before_action :authenticate_account!, only: :index

  def index
    # @charge = Charge.new
  end

  def wallet
    @verify_btcs = current_account.btcs.where(status: [0, 1])
  end

  def history
    params[:type] ||= 'btc'
    @wallets = case params[:type]
               when 'ltc'
                 current_account.ltcs
               when 'eth'
                 current_account.eths
               else
                 current_account.btcs
               end
    @wallets = current_account.btcs.order(id: :desc)
    @wallets = @wallets.page params[:page]
  end

  def purchase
    if request.xhr?
      if params[:token] && params[:type] == 'get_purchase'
        @btc  = params[:token].to_d / BtcAddress::TOKEN_UNIT
        @code = current_account.purchase_btc_wallet_code
      elsif params[:type] == 'purchase'
        @result = current_account.purchase_btc(params[:code], params[:amount])
      end
    end
  end

  def status
    @transaction = Transaction.find params[:id]
    if @transaction.confirm? && params[:t] == 'confirm'
      @transaction.finish!
    end
  end

  def txes
    @txes = Tx.where(address: params[:address]).page(params[:page]).per 10
  end
end
