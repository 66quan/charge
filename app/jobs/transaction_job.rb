class TransactionJob < ApplicationJob
  queue_as :default

  def perform(id)
    transaction = Transaction.find id
    transaction.fresh
    # Do something later
  end
end
