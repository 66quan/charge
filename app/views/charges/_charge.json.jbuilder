json.extract! charge, :id, :account_id, :amount, :wallet_type, :created_at, :updated_at
json.url charge_url(charge, format: :json)
